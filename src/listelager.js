import { datahenter } from './datahenter.js';
import { tabellager } from './tabellager.js';

function lagInfoboks() {
  let data = datahenter.historikk;
  infofelt.innerHTML = '<b>Data gjelder fra ' + data[0].endTime + ' til ' + data[data.length - 1].endTime + '</b>';
}

function lagSangtabell() {
  let sangtabell = document.getElementById('sangtabell');
  if (sangtabell.firstChild) sangtabell.removeChild(sangtabell.firstChild);

  let sanger = datahenter.hentSanger();
  sangtabell.appendChild(tabellager.lagTabell(sanger, antallsanger.value));
}

function lagArtisttabell() {
  let artisttabell = document.getElementById('artisttabell');
  if (artisttabell.firstChild) artisttabell.removeChild(artisttabell.firstChild);

  let artister = datahenter.hentArtister();
  artisttabell.appendChild(tabellager.lagTabell(artister, antallartister.value));
}

lagInfoboks();
lagSangtabell();
lagArtisttabell();

sorterPå.onchange = () => {
  lagSangtabell();
};
antallartister.onchange = () => {
  lagArtisttabell();
};
starttidartister.onchange = () => {
  lagArtisttabell();
};
antallsanger.onchange = () => {
  lagSangtabell();
};
starttidsanger.onchange = () => {
  lagSangtabell();
};
