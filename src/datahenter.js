import fs from 'fs';
const STI = 'src/historikk';
class Datahenter {
  constructor() {
    this.sorterPå = 'spilletid';
    this.historikk = this.hentHistorikk();
  }
  hentHistorikk() {
    let historikk = [];
    let filer = fs.readdirSync(STI);
    for (let fil of filer) {
      try {
        let data = fs.readFileSync(STI + '/' + fil);
        historikk = historikk.concat(JSON.parse(data));
      } catch (e) {}
    }
    return historikk;
  }
  hentArtister() {
    let startdato = Date.parse(starttidartister.value);
    let artister = [];
    for (let sang of this.historikk) {
      if (Date.parse(sang.endTime) < startdato) continue;
      //finn indeks i artister for artisten
      let pos = artister
        .map(function(e) {
          return e.navn;
        })
        .indexOf(sang.artistName);

      if (pos == -1) {
        artister.push({ navn: sang.artistName, avspillinger: 1 });
      } else {
        ++artister[pos].avspillinger;
      }
    }

    //sorter på avspillinger
    artister.sort((a, b) => (a.avspillinger < b.avspillinger ? 1 : -1));

    return artister;
  }
  hentSanger() {
    this.sorterPå = document.getElementById('sorterPå').value;
    let startdato = Date.parse(starttidsanger.value);
    let sanger = [];
    for (let sang of this.historikk) {
      if (Date.parse(sang.endTime) < startdato) continue;
      //finn indeks i artister for artisten
      let pos = sanger
        .map(function(e) {
          return e.navn;
        })
        .indexOf(sang.trackName);
      if (pos == -1) {
        sanger.push({ navn: sang.trackName, artist: sang.artistName, avspillinger: 1, spilletid: sang.msPlayed });
      } else {
        ++sanger[pos].avspillinger;
        sanger[pos].spilletid += sang.msPlayed;
      }
    }

    //sorter på avspillinger
    switch (this.sorterPå) {
      case 'spilletid':
        sanger.sort((a, b) => (a.spilletid < b.spilletid ? 1 : -1));
        break;
      case 'avspillinger':
        sanger.sort((a, b) => (a.avspillinger < b.avspillinger ? 1 : -1));
        break;
    }

    //konverter fra ms
    for (let sang of sanger) {
      let gammeltid = sang.spilletid;
      sang.spilletid = '' + this.msTilTid(sang.spilletid);
    }
    return sanger;
  }
  msTilTid(duration) {
    var milliseconds = parseInt((duration % 1000) / 100),
      seconds = Math.floor((duration / 1000) % 60),
      minutes = Math.floor((duration / (1000 * 60)) % 60),
      hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    return hours + ':' + minutes + ':' + seconds;
  }
}
export let datahenter = new Datahenter();
