class Tabellager {
  lagTabell(data, antallrader) {
    let tabell = document.createElement('table');
    let rad = '';
    let tr = document.createElement('tr');

    //går gjennom attributtnavnene fra tabellen og lager en header.
    for (let i = 0; i < Object.getOwnPropertyNames(data[0]).length; i++) {
      rad += '<th>' + Object.getOwnPropertyNames(data[0])[i] + '</th>';
    }
    tr.innerHTML = rad;
    tabell.appendChild(tr);

    //går gjennom hver rad i sqldataen og lager rader og fyller ut celler med riktig innhold
    for (const [i, sang] of data.entries()) {
      rad = '';
      if (i > antallrader) break;
      tr = document.createElement('tr');
      for (let i = 0; i < Object.getOwnPropertyNames(data[0]).length; i++) {
        //formaterer dato til et leselig format om innholdet er en dato
        rad += '<td>' + sang[Object.getOwnPropertyNames(data[0])[i]] + '</td>';
      }
      tr.innerHTML = rad;
      tabell.appendChild(tr);
    }
    return tabell;
  }
}

export let tabellager = new Tabellager();
