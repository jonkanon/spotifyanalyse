const electron = require('electron');
const path = require('path');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

// Reload application on changes in src folder
require('electron-reload')(path.join(__dirname, 'src'), {
  electron: path.join(__dirname, 'node_modules/.bin/electron'),
  ignored: /^.*\.(json|txt)$/
});

let mainWindow;
app.on('ready', () => {
  mainWindow = new BrowserWindow({ width: 800, height: 600, webPreferences: { nodeIntegration: true } });

  // Open Development Tools
  // mainWindow.openDevTools();

  mainWindow.loadURL('file://' + __dirname + '/src/index.html');
});

app.on('window-all-closed', () => {
  app.quit();
});
